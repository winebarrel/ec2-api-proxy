$: << File.expand_path(File.dirname(__FILE__) + '/lib')
require 'ec2-api-proxy/constants'

Gem::Specification.new do |spec|
  spec.name              = 'ec2-api-proxy'
  spec.version           = Version
  spec.summary           = 'ec2-api-proxy is a proxy server for EC2 API.'
  spec.require_paths     = %w(lib)
  spec.files             = %w(README) + Dir.glob('bin/**/*') + Dir.glob('lib/**/*')
  spec.author            = 'winebarrel'
  spec.email             = 'sgwr_dts@yahoo.co.jp'
  spec.homepage          = 'https://bitbucket.org/winebarrel/ec2-api-proxy'
  spec.bindir            = 'bin'
  spec.executables << 'ec2-api-proxy'
  spec.executables << 'eapctl'
  spec.add_dependency('eventmachine', '~> 1.0.3')
  spec.add_dependency('rexec', '~> 1.5.1')
  spec.add_dependency('dalli', '~> 2.6.4')
end
