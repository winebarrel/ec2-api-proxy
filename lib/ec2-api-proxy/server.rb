require 'drb/drb'
require 'fileutils'
require 'eventmachine'
require 'rexec'
require 'rexec/daemon'
require 'dalli'

require 'ec2-api-proxy/proxy'

module RExec; module Daemon; class Base
  def self.daemon_name; APP_NAME; end
end; end; end

module EC2APIProxy
  class Server < RExec::Daemon::Base

    class << self
      def options=(options)
        @@options = options
        @@base_directory = options[:working_dir]
      end

      def run
        @@control_options = {
          :memcached => @@options[:memcached],
          :expires   => @@options[:expires],
          :compress  => @@options[:compress],
          :use_https => @@options[:use_https],
          :debug     => @@options[:debug],
        }

        # start DRb
        FileUtils.rm_f(@@options[:socket])
        DRb.start_service("drbunix:#{@@options[:socket]}", @@control_options)
        File.chmod(0700, @@options[:socket])
        at_exit { FileUtils.rm_f(@@options[:socket]) }

        EM.epoll
        EM.threadpool_size = @@options[:threads] if @@options[:threads]

        EM.run {
          memcached = Dalli::Client.new(@@options[:memcached], :compress => @@options[:compress])
          EM.start_server(@@options[:addr], @@options[:port], EC2APIProxy::Proxy, @@control_options, memcached)
        }
      end
    end # self

  end # Server
end # EC2APIProxy
