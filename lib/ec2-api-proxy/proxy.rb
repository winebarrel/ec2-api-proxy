require 'eventmachine'
require 'ec2-api-proxy/backend'
require 'logger'
require 'uri'
require 'cgi'

module EC2APIProxy
  class Proxy < EM::Connection
    attr_reader :options
    attr_reader :logger
    attr_reader :memcached

    def initialize(options, memcached)
      @options = options
      @memcached = memcached
      @logger = Logger.new($stdout)
      @data_len = nil
      @data = ''
    end

    def post_init
      if (peername = get_peername)
        @connect_from = Socket.unpack_sockaddr_in(peername)
      end

      if @options[:debug]
        port, ip = @connect_from
        @logger.debug("proxy: connect from #{ip}:#{port}")
      end
    end

    def unbind
      close_backend_connection

      if @options[:debug]
        port, ip = @connect_from
        @logger.debug("proxy: unbind connection from #{ip}:#{port}")
      end
    end

    def receive_data(data)
      @data << data

      unless @data_len
        content_length = data.split("\r\n").find {|i| /\AContent-Length:/i =~ i }
        @data_len = content_length ? content_length.split(/:\s+/, 2).last.to_i : 0
      end

      content = @data.split("\r\n\r\n", 2).last || ''
      @logger.debug("proxy: receive data (length=#{@data_len}, receive=#{content.length})") if @options[:debug]
      return if content.length < @data_len

      EM.defer {
        begin
          endpoint, params = parse_proxy_request(@data)
          action = (params['Action'] || []).first
          allow_cache = !!(/\ADescribe/ =~ action)
          key = params_to_key(params)

          @logger.debug("proxy: #{action} (allow_cache=#{allow_cache})") if @options[:debug]

          if (allow_cache and cache = @memcached.get(key))
            @logger.debug("proxy: cache hit (length=#{cache.length})") if @options[:debug]
            send_data(cache)
            close_connection_after_writing
          else
            @logger.debug("proxy: cache miss") if @options[:debug]
            @backend = EM.connect(
                         endpoint.host,
                         @options[:use_https] ? 443 : endpoint.port,
                         EC2APIProxy::Backend,
                         self,
                         allow_cache ? key : nil
                       )

            if @backend.error?
              @logger.error("proxy: backend connection error: #{@data.inspect}")
              send_error('ec2-api-proxy-backend-connection-error', 'backend connection error')
              close_backend_connection
              close_connection_after_writing
            else
              @backend.send_data(@data)
            end
          end
        rescue => e
          @logger.error("#{e.class.name}: #{e.message}\n\tfrom #{e.backtrace.join("\n\tfrom ")}")
          send_error(e.class.name, e.message)
          close_backend_connection
          close_connection_after_writing
        end
      }
    end

    private

    def parse_proxy_request(data)
      request_line, header_body = data.split("\r\n", 2)
      request_method, endpoint, http_version = request_line.split(/\s+/, 3)
      endpoint = URI.parse(endpoint)
      params = nil

      if request_method =~ /GET/i
        params = CGI.parse(endpoint.query)
      else
        params = CGI.parse(header_body.split(/\r\n\r\n/, 2).last)
      end

      [endpoint, params]
    end

    def params_to_key(params)
      params = params.dup

      %w(
        SignatureVersion
        AWSAccessKeyId
        Timestamp
        SignatureMethod
        Version
        Signature
      ).each {|i| params.delete(i) }

      params.sort.map {|k, v| "#{k}=#{v}" }.join('&')
    end

    def send_error(code, message)
      send_data(<<-EOS)
<Response>
    <Errors>
         <Error>
           <Code>#{code}</Code>
           <Message>#{message}</Message>
         </Error>
    </Errors>
</Response>
      EOS
    end

    def close_backend_connection
      @backend.close_connection if @backend
    rescue Exception => e
      @logger.warn("#{e.class.name}: #{e.message}\n\tfrom #{e.backtrace.join("\n\tfrom ")}")
    end
  end # Proxy
end # EC2APIProxy
