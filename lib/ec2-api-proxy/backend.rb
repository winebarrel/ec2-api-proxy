require 'eventmachine'

module EC2APIProxy
  class Backend < EM::Connection
    def initialize(proxy, memcached_key)
      @proxy = proxy
      @logger = proxy.logger
      @memcached_key = memcached_key
      @data_list = []
    end

    def unbind
      if @proxy and @proxy.options[:debug]
        @logger.debug("backend: unbind connection")
      end

      if @memcached_key and not @data_list.empty?
        data = @data_list.join

        if /<Response>\s*<Errors>\s*<Error>/ =~ data
          if @proxy and @proxy.options[:debug]
            @logger.debug("backend: error response: #{data}")
          end
        else
          @proxy.memcached.set(@memcached_key, data, @proxy.options[:expires])
        end

        if @proxy and @proxy.options[:debug]
          @logger.debug("backend: set cache (length=#{data.length})")
        end
      end
    end

    def receive_data(data)
      if not @proxy or @proxy.error?
        @logger.error("backend: proxy connection error: #{data.inspect}")
        close_proxy_connection
        close_connection_after_writing
      else
        @data_list << data
        @proxy.send_data(data)
      end
    end

    private

    def close_proxy_connection
      @proxy.close_connection if @proxy
    rescue Exception => e
      @logger.warn("#{e.class.name}: #{e.message}\n\tfrom #{e.backtrace.join("\n\tfrom ")}")
    end
  end # Backend
end # EC2APIProxy
